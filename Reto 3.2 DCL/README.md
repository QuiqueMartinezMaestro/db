# Reto 3.2 DCL - Control de Acceso.

Quique Martinez Maestro.

En este reto 3.2 vamos a tratar el DCL(Data Control Language) que es el conjunto de comandos utilizados para controlar los permisos de acceso a los datos en una base de datos. Probaremos los comandos propuestos en este documento con la BD `Chinook`.

He podido obtener información adicional del siguiente enlace propuesto: https://dev.mysql.com/doc/refman/8.3/en/access-control.html

Mis Querys se encuentran el el siguiente enlace:https://gitlab.com/QuiqueMartinezMaestro/db/-/tree/main/Reto%203.2%20DCL?

Para poder poder aplicar estos comandos ,primero tenemos que saber que BD tenemos en nuestro servidor y despues de saber cuales tenemos debemos conectarnos de manera correcta para esto deberemos aplicar los 2 siguientes comandos:

* Como ver las BD de tu SGBD : `Show database.`

* Conectar con una BD : `mysql usuario -u host -h -P puerto -p contraseña.`

Cómo registrar nuevos usuarios, modificarlos y eliminarlos:

* Crear un usuario(esteban quito): `CREATE USER 'esteban quito'@'192.168.4%' IDENTIFIED BY '1234'`

* Modificar un Usuario (Host): `RENAME USER 'esteban quito'@'192.168.4%' TO 'esteban quito'@'%'`

* Como eliminar un usuario(esteban quito):`DROP USER 'esteban quito'@'%';`

Cómo se autentican los usuarios y qué opciones de autenticación nos ofrece el SGBD:

En mi caso yo estoy utilizando la primera forma de autentificación `Password Authentication`, como se puede ver en la creación del usuario, pero hay 3 formas con las que se puede llevar a cabo la autentificación

* Autenticación basada en autenticación nativa de MySQL (Native MySQL Authentication).

* Autenticación basada en autenticación externa (External Authentication).

* Autenticación basada en certificados SSL/TLS.

Cómo mostrar los usuarios existentes y sus permisos:

* Como ver los usuarios de la BD : `SELECT host,user FROM mysql.user.`

* Como ver los permisos de un usuario (esteban quito) `SHOW GRANTS FOR 'esteban quito' '@' '%'.`


Qué permisos puede tener un usuario y qué nivel de granularidad nos ofrece el SGBD:

Los permisos que nos ofrece mySQL son bastente granulados ya que puedes otorgar permisos a nivel BD, tabla y incluso columnas específicas.
Los permisos para gestionar BD mas comunes son: `SELECT`, `INSERT`,`UPDATE`,`DELETE`,`CREATE`,`DROP`,`ALL PRIVILEGES.`

* Como puedes añadir permisos a un usuario para gestionar las BD(esteban quito): `GRANT DELETE ON Chinook.* TO 'esteban quito'@'%';`

* Como puedes eliminar permisos a un usuario  para gestionar las BD(esteban quito): `REVOKE DELETE  ON Chinook.* FROM 'esteban quito'@'%';`


Qué permisos hacen falta para gestionar los usuarios y sus permisos:

Para gestionar usuarios y sus permisos , se requieren ciertos permisos especiales que normalmente son otorgados a los usuarios con roles mas altos.
Los permisos para gestionar usuarios mas comunes son: `CREATE USER`, `ALTER USER`,`DROP USER`,`GRANT OPTION`,`REVOKE`

* Como puedes añadir permisos a un usuario para gestionar usuarios(esteban quito): `GRANT CREATE USER ON Chinook.* TO 'esteban quito'@'%';`

* Como puedes eliminar permisos a un usuario  para gestionar usuarios(esteban quito): `REVOKE CREATE USER  ON Chinook.* FROM 'esteban quito'@'%';`


Posibilidad de agrupar los usuarios (grupos/roles/...).

* Crear un rol: `CREATE ROLE Usuario;`

* Añadir un usuario a un rol: `GRANT Usuario TO 'esteban quito'@'%';`

* Como ver los roles que hay en mi BD: `GRANT ALL PRIVILEGES ON Chinook.* TO 'Usuario'@'%';`























