SELECT  Titol AS "Título de la película" , ACTOR.NOM AS "Actor Principal"
FROM PELICULA JOIN ACTOR
ON PELICULA.CodiActor = ACTOR.CodiActor;