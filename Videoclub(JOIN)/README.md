# Reto 1: Consultas básicas

Quique Martínez Maestro.

En este reto trabajamos con la base de datos ` `videoclub`,  que nos viene dada en el fichero  `videoclub`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

Mis Querys se encuentran el el siguiente enlace:https://gitlab.com/QuiqueMartinezMaestro/db/-/tree/main/Videoclub(JOIN)


## Query 1
Para listar todas las peliculas junto a su género deberemos utilixzar el operador `JOIN` para unir las dos tablas `Pelicula` y `Genero` : `PELICULA JOIN GENERE` ademas deberemos filtrar con el oprador `ON` los `CodiGenere` para saber los generos de las peliculas a través de su codigo.Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT	CodiPeli, Titol , Descripcio
FROM	PELICULA JOIN GENERE
ON	GENERE.CodiGenere = PELICULA.CodiGenere;
```

## Query 2
Para saber todas las facturas de Maria deberemos utilizar el oprador `JOIN` para unir las dos tablas `Factura` y `Client`: `FACTURA JOIN CLIENT` deberemos filtrar con el operador `ON` el `DNI`
ademas en el `WHERE` deberemos especificar que el nombre de el cliente sea Maria con el operador `LIKE`.Lo llevaremos a cabo con la siguiente sentencia SQL:


```sql
SELECT	FACTURA.* , CLIENT.NOM
FROM	FACTURA JOIN CLIENT
ON		FACTURA.DNI = CLIENT.DNI
WHERE	CLIENT.NOM LIKE 'MARIA%';
```

## Query 3
Para listar todas las peliculas junto a su actor principal deberemos utilixzar el operador `JOIN` para unir las dos tablas `Pelicula` y `ACTOR` : `PELICULA JOIN ACTOR` ademas deberemos filtrar con el oprador `ON` los `CodiActor` para saber los actores principales de las peliculas a través de su codigo.Lo llevaremos a cabo con la siguiente sentencia SQL:


```sql
SELECT  Titol AS "Título de la película" , ACTOR.NOM AS "Actor Principal"
FROM PELICULA JOIN ACTOR
ON PELICULA.CodiActor = ACTOR.CodiActor;
```

## Query 4
Para listar todas las peliculas junto a los actores que la han interpretado deberemos utilixzar el operador `JOIN` dos veces  para unir las tres tablas tablas `Pelicula` y `ACTOR`  y `INTERPRETADA`: `PELICULA JOIN ACTOR JOIN  INTERPRETADA` ademas deberemos filtrar con el oprador `ON` los `CodiActor` y los `CodiPeli` para saber los actores que la han interpretado alguna vez las peliculas a través de sus codigos.Lo llevaremos a cabo con la siguiente sentencia SQL:


```sql
SELECT 	Titol AS "Título de la película" , ACTOR.NOM AS "Actor"
FROM 	PELICULA JOIN INTERPRETADA JOIN ACTOR
ON PELICULA.CodiPeli = INTERPRETADA.Codipeli AND
INTERPRETADA.CodiActor = ACTOR.CodiActor
```

## Query 5

```sql

```

