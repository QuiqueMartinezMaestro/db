SELECT a.*
FROM album a
WHERE (
    SELECT SUM(t.Milliseconds)
    FROM track t
    WHERE t.AlbumId = a.AlbumId
) > (
    SELECT AVG(album_durations.total_duration)
    FROM (
        SELECT AlbumId, SUM(Milliseconds) AS total_duration
        FROM track
        GROUP BY AlbumId
    ) AS album_durations
);