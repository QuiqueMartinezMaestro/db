SELECT t.*
FROM track t
JOIN playlisttrack pt ON t.TrackId = pt.TrackId
WHERE pt.PlaylistId = (
    SELECT PlaylistId
    FROM (
        SELECT PlaylistId
        FROM playlisttrack
        GROUP BY PlaylistId
        ORDER BY COUNT(*) DESC
        LIMIT 1
    ) AS subquery
);