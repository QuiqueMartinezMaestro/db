SELECT *
FROM track
WHERE GenreId = (
    SELECT GenreId
    FROM track
    GROUP BY GenreId
    ORDER BY COUNT(*) DESC
    LIMIT 1
);