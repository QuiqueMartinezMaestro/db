SELECT a.*
FROM album a
WHERE (
    SELECT COUNT(*)
    FROM track c
    WHERE c.AlbumId = a.AlbumId
) > (
    SELECT AVG(canciones_por_album.num_canciones)
    FROM (
        SELECT COUNT(*) AS num_canciones
        FROM track
        GROUP BY AlbumId
    ) AS canciones_por_album
);