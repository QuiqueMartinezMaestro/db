SELECT a.ArtistId, ar.Name AS ArtistName,
       (SELECT al.Title
        FROM album al
        WHERE al.ArtistId = a.ArtistId
        ORDER BY al.AlbumId DESC
        LIMIT 1) AS LatestAlbum
FROM artist ar
JOIN album a ON ar.ArtistId = a.ArtistId
GROUP BY a.ArtistId, ar.Name;