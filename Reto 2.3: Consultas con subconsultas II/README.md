# Reto 2.3: Consultas con subconsultas II
**[Bases de Datos] Unidad Didáctica 2: DML - Consultas Avanzadas**

Quique


Para este reto, volveremos a usar la base de datos Chinook (más información en el Reto 2.1).

![Diagrama relacional de Chinook (fuente: github.com/lerocha/chinook-database).](https://github.com/lerocha/chinook-database/assets/135025/cea7a05a-5c36-40cd-84c7-488307a123f4)

Mis Querys se encuentran el el siguiente enlace: https://gitlab.com/QuiqueMartinezMaestro/db/-/tree/main/Reto%202.3%3A%20Consultas%20con%20subconsultas%20II

Tras cargar esta base de datos en tu SGBD, realiza las siguientes consultas:

## Subconsultas Escalares (Scalar Subqueries)
Estas subconsultas devuelven un solo valor, por lo general, se utilizan en contextos donde se espera un solo valor, como parte de una condición `WHERE`, `SELECT`, `HAVING`, etc.
Ejemplo:

_Obtener una lista de empleados que ganan más que el salario medio de la empresa. [1]_

```sql
SELECT
  lastname,
  firstname,
  salary
FROM employee
WHERE salary > (SELECT avg(salary)
                FROM employee)
```

### Consulta 1

La subconsulta calcula la media de `milliseconds`de `track`, y la compara con la primera consulta,
que selecciona las canciones que son superiores a esta media.

```sql
SELECT *
FROM track
WHERE milliseconds > (SELECT AVG(milliseconds) FROM track);
```

### Consulta 2

La subconsulta encuentra el `CustomerId` del ``Email`emma_jones@hotmail.com` y la consulta principal lista 
las 5 ultimas facturas que coinciden con el `CustomerId`obtenido en la subconsulta.

```sql
SELECT *
FROM invoice
WHERE CustomerId = (SELECT CustomerId FROM customer WHERE Email = 'emma_jones@hotmail.com')
ORDER BY InvoiceDate DESC
LIMIT 5;
```

## Subconsultas de varias filas

Diferenciamos dos tipos:

1. Subconsultas que devuelven una columna con múltiples filas (es decir, una lista de valores). Suelen incluirse en la cláusula `WHERE` para filtrar los resultados de la consulta principal. En este caso, suelen utilizarse con operadores como `IN`, `NOT IN`, `ANY`, `ALL`, `EXISTS` o `NOT EXISTS`.
2. Subconsultas que devuelven múltiples columnas con múltiples filas (es decir, tablas). Se comportan como una tabla temporal y se utilizan en lugares donde se espera una tabla, como en una cláusula `FROM`. [2]

### Consulta 3
Mostrar las listas de reproducción en las que hay canciones de reggae.

PRIMERA OPCIÓN(INNER JOIN).

La segunda subconsulta obtiene el `GenreId` relacionado con el nombre reggae, la primera subconsulta selecciona las`Playlist` de la tabla `Track` en las que coincide el `GenreId` obtenido y la consulta principal selleciona las `Playlist` donde hay un `Track` que tiene el `GenreID`
que hemos obtenido en la segunda subconsulta.

```sql
SELECT *
FROM playlist
WHERE PlaylistId IN (
    SELECT PlaylistId 
    FROM track 
    WHERE GenreId IN (
        SELECT GenreId
        FROM genre 
        WHERE Name = 'reggae'
    )
);
```
SEGUNDA OPCION(INNER JOIN).

```sql
SELECT *
FROM playlist
WHERE PlaylistId IN (SELECT PlaylistId FROM track t INNER JOIN genre g ON t.GenreId = g.GenreId WHERE g.Name = 'reggae');
```



### Consulta 4
Obtener la información de los clientes que han realizado compras superiores a 20€.

La subconsulta filtra los `CustomerId` de los que han realizado compras superiores a 20€ y la cosulta principal 
selecciona los clientes que su `CustomerId` coincide con el de la subconsulta.

```sql
SELECT *
FROM customer
WHERE CustomerId IN (SELECT CustomerId FROM invoice WHERE Total > 20);
```

### Consulta 5
Álbumes que tienen más de 15 canciones, junto a su artista.

La subconsulta selecciona las cuanta las canciones que que aparecen un un album filtrando por `AlbumId`, 
la consulta principal utiliza el operador `Join` para saber los nombres de los artistas utilizando el `ArtistId`.

```sql
SELECT a.Title, ar.Name
FROM album a
JOIN artist ar ON a.ArtistId = ar.ArtistId
WHERE (SELECT COUNT(*) FROM track WHERE AlbumId = a.AlbumId) > 15;
```


### Consulta 6
Obtener los álbumes con un número de canciones superiores a la media.

```sql
SELECT a.*
FROM album a
WHERE (
    SELECT COUNT(*)
    FROM track c
    WHERE c.AlbumId = a.AlbumId
) > (
    SELECT AVG(canciones_por_album.num_canciones)
    FROM (
        SELECT COUNT(*) AS num_canciones
        FROM track
        GROUP BY AlbumId
    ) AS canciones_por_album
);
```

### Consulta 7
Obtener los álbumes con una duración total superior a la media.

```sql
SELECT a.*
FROM album a
WHERE (
    SELECT SUM(t.Milliseconds)
    FROM track t
    WHERE t.AlbumId = a.AlbumId
) > (
    SELECT AVG(album_durations.total_duration)
    FROM (
        SELECT AlbumId, SUM(Milliseconds) AS total_duration
        FROM track
        GROUP BY AlbumId
    ) AS album_durations
);
```

### Consulta 8
Canciones del género con más canciones.

La subconsulta determina cual es el `GenreId` con mas canciones y la consulta selecciona todos los datos del Genero
con el mismo `GenreId`.

```sql
SELECT *
FROM track
WHERE GenreId = (
    SELECT GenreId
    FROM track
    GROUP BY GenreId
    ORDER BY COUNT(*) DESC
    LIMIT 1
);


```

### Consulta 9
Canciones de la playlist con más canciones.

La subconsulta obtenie el el `PlaylistId` de la playlist con mas canciones haciendo un `COUNT`
y un `LIMIT` y la primera consulta lo que hace es seleccionar las canciones que estan en la playlits con
el `PlaylistId` que hemos obtenido en la subconsulta.

```sql
SELECT t.*
FROM track t
JOIN playlisttrack pt ON t.TrackId = pt.TrackId
WHERE pt.PlaylistId = (
    SELECT PlaylistId
    FROM (
        SELECT PlaylistId
        FROM playlisttrack
        GROUP BY PlaylistId
        ORDER BY COUNT(*) DESC
        LIMIT 1
    ) AS subquery
);
```


## Subconsultas Correlacionadas (Correlated Subqueries):
Son subconsultas en las que la subconsulta interna depende de la consulta externa. Esto significa que la subconsulta se ejecuta una vez por cada fila devuelta por la consulta externa, suponiendo una gran carga computacional.
Ejemplo:

_Supongamos que queremos encontrar a todos los empleados con un salario superior al promedio de su departamento. [1]_

```sql
SELECT
  lastname,
  firstname,
  salary
FROM employee e1
WHERE e1.salary > (SELECT avg(salary)
                   FROM employee e2
                   WHERE e2.dept_id = e1.dept_id)
```

La principal diferencia entre una subconsulta correlacionada en SQL y una subconsulta simple es que las subconsultas correlacionadas hacen referencia a columnas de la tabla externa. En el ejemplo anterior, `e1.dept_id` es una referencia a la tabla de la subconsulta externa. [1]

### Consulta 10
Muestra los clientes junto con la cantidad total de dinero gastado por cada uno en compras.

La subconsulta lo que hace la suma del dinero gastado que tienen los clientes filtrado por `CustomerId`,la primera 
consulta llama a esta subconsulta a traves de `c.CustomerId`

```sql
SELECT c.CustomerId, c.FirstName, c.LastName,
       (SELECT SUM(i.Total)
        FROM invoice i
        WHERE i.CustomerId = c.CustomerId) AS TotalGastado
FROM customer c;
```

### Consulta 11
Obtener empleados y el número de clientes a los que sirve cada uno de ellos.

La subconsulta cuenta el numero de clientes a los que sirve cada empleado y luego la consulta principal llama a esta
subconsulta a través de `e.EmployeeId`.

```sql
SELECT e.EmployeeId, e.FirstName, e.LastName,
       (SELECT COUNT(DISTINCT c.CustomerId)
        FROM customer c
        WHERE c.SupportRepId = e.EmployeeId) AS NumClientsServed
FROM employee e;
```

### Consulta 12
Ventas totales de cada empleado.

La primera subconsulta relaciona todos los `Customers` con los `Employees`, la segunda suma el total de dinero gastado
de los customers y la consulta se relaciona con las subconsultas a traves de `e.EmployeeId`

```sql
SELECT e.EmployeeId, e.FirstName, e.LastName,
       (SELECT SUM(i.Total)
        FROM invoice i
        WHERE i.CustomerId IN (
            SELECT c.CustomerId
            FROM customer c
            WHERE c.SupportRepId = e.EmployeeId
        )) AS TotalSales
FROM employee e;
```

### Consulta 13
Álbumes junto al número de canciones en cada uno.

```sql
SELECT a.AlbumId, a.Title,
       (SELECT COUNT(*)
        FROM track t
        WHERE t.AlbumId = a.AlbumId) AS NumberOfSongs
FROM album a;
```

### Consulta 14
Obtener el nombre del álbum más reciente de cada artista. Pista: los álbumes más antiguos tienen un ID más bajo.

```sql
SELECT a.ArtistId, ar.Name AS ArtistName,
       (SELECT al.Title
        FROM album al
        WHERE al.ArtistId = a.ArtistId
        ORDER BY al.AlbumId DESC
        LIMIT 1) AS LatestAlbum
FROM artist ar
JOIN album a ON ar.ArtistId = a.ArtistId
GROUP BY a.ArtistId, ar.Name;
```


## Referencias
- [1] https://learnsql.es/blog/subconsulta-correlacionada-en-sql-una-guia-para-principiantes/
- [2] https://learnsql.es/blog/cuales-son-los-diferentes-tipos-de-subconsultas-sql/

