SELECT e.EmployeeId, e.FirstName, e.LastName,
       (SELECT COUNT(DISTINCT c.CustomerId)
        FROM customer c
        WHERE c.SupportRepId = e.EmployeeId) AS NumClientsServed
FROM employee e;