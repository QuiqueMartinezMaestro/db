SELECT a.Title, ar.Name
FROM album a
JOIN artist ar ON a.ArtistId = ar.ArtistId
WHERE (SELECT COUNT(*) FROM track WHERE AlbumId = a.AlbumId) > 15;