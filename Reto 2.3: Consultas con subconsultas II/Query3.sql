SELECT *
FROM playlist
WHERE PlaylistId IN (
    SELECT PlaylistId 
    FROM track 
    WHERE GenreId IN (
        SELECT GenreId
        FROM genre 
        WHERE Name = 'reggae'
    )
);