SELECT e.EmployeeId, e.FirstName, e.LastName,
       (SELECT SUM(i.Total)
        FROM invoice i
        WHERE i.CustomerId IN (
            SELECT c.CustomerId
            FROM customer c
            WHERE c.SupportRepId = e.EmployeeId
        )) AS TotalSales
FROM employee e;