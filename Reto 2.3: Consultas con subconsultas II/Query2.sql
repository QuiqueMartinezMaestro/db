SELECT *
FROM invoice
WHERE CustomerId = (SELECT CustomerId FROM customer WHERE Email = 'emma_jones@hotmail.com')
ORDER BY InvoiceDate DESC
LIMIT 5;