SELECT a.AlbumId, a.Title,
       (SELECT COUNT(*)
        FROM track t
        WHERE t.AlbumId = a.AlbumId) AS NumberOfSongs
FROM album a;