SELECT c.CustomerId, c.FirstName, c.LastName,
       (SELECT SUM(i.Total)
        FROM invoice i
        WHERE i.CustomerId = c.CustomerId) AS TotalGastado
FROM customer c;