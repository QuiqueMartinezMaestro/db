# Reto 1: Consultas básicas

Quique Martínez Maestro.

En este reto trabajamos con la base de datos  `Chinook`,  que nos viene dada en el fichero  `Chinook_MySql`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

![Chinook](Chinook.png)


Mis Querys se encuentran el el siguiente enlace: https://gitlab.com/QuiqueMartinezMaestro/db/-/tree/main/Reto%202.1%20Consultas%20Agregaci%C3%B3n


# Consultas sobre una Tabla

## Query 1

Para saber todos los clientes de Francia en el operador `WHERE` deberemos pedir que la columna `Country` sea France.


```sql
SELECT CustomerId, FirstName, LastName, Country
FROM Customer
WHERE Country = 'France';
```

## Query 2

Para sacar las facturas del primer trimestre filtraremos con el operador `MONTH` al cual le asignaremos los primeros tres meses del año en formato numerico (1:ENERO), (2:FEBRERO), (3:MARZO).

```sql
SELECT InvoiceId, InvoiceDate
FROM Invoice
WHERE MONTH(InvoiceDate) in (1, 2 ,3);
```

## Query 3

Para saber todas las canciones de AC/DC en el operador `WHERE` deberemos pedir que la columna `COMPOSER` sea AC/DC.


```sql
SELECT TrackId, Name
FROM Track
WHERE Composer = 'AC/DC';
```

## Query 4

Utilyzaremos el operador `ORDER BY DESC` para ordenar de manera descendente las canciones que mas ocupan , acompañado del operador `LIMIT` en el que le añadimos el numero de canciones que queremos saber, en nuestro caso 10. 

```sql
SELECT TrackId, Name
FROM Track
ORDER BY Bytes DESC
LIMIT 10;
```
## Query 5

Consulta muy sencilla ya que todos los clientes tienen asiganados un pais por lo tanto lo unico que utilizamos es el operador `DISTINCT` para que no se repitan los nombres de los paises.

```sql
SELECT DISTINCT Country
FROM Customer;
```

## Query 6

Mostramos todos los géneros musicales .

```sql
SELECT * 
FROM Genre;
```

# Consultas sobre múltiples tablas

## Query 7

Utilizamos el operador `JOIN` y filtramos por `ArtistaId` para saber los artistas junto a sus albumes.

```sql
SELECT Artist.name , Album.Title
FROM Artist
JOIN Album ON Album.ArtistId = Artist.ArtistId;

```
## Query 8

Utilizamos el operador ` LEFT JOIN` en la misma tabla para filtrar por `EmployeeId` y `ReportsTo` para saber que empleados tienen supervisores, además utilizamos los operadores `ORDER BY` y  `LIMIT` para saber los 15 empleados mas jovenes.

```sql
SELECT  e1.FirstName AS Empleado,  e2.FirstName AS Supervisor 
FROM Employee AS e1
LEFT JOIN Employee AS e2 ON e1.EmployeeId = e2.ReportsTo
ORDER BY e1.BirthDate ASC
LIMIT 15;
```

## Query 9

Utilizamos el operador `JOIN` y filtramos por `CustomerId` , tambien usamos el operador `WHERE` , deberemos pedir que la columna `c.City` sea Berlin, de esta manera sacaremos todos los clientes berlineses.


```sql
SELECT i.InvoiceDate,
 c.FirstName AS Nombre, 
 c.LastName AS Apellido,
 i.BillingAddress AS DirecFact,
 i.BillingPostalCode AS CodPostal,
 i.BillingCountry AS Pais,
 i.total AS Importe
FROM Customer AS c
JOIN Invoice AS i ON c.CustomerId = i.CustomerId
Where c.City = 'Berlin';
```
## Query 10

Utilizamos 3 operadores `JOIN` para saber las `Playlist` junto a sus `Tracks` y a que `Album` pertenecen , ademas dicha lista de reproducción debe empezar por `C` por lo tanto utilizamos el operador `LIKE` para filtrar, por ultimo ordenamos por `Title` y duración `(Milliseconds)`.

```sql
SELECT p.Name AS 'Lista de Reproduccion', t.Name AS Cancion , a.Title AS ALBUM 
FROM Playlist AS p 
JOIN PlaylistTrack AS pt ON p.PlaylistId = pt.PlaylistId
JOIN Track AS t ON pt.TrackId = t.TrackId
JOIN Album AS a ON a.AlbumId = t.AlbumID
WHERE p.Name LIKE 'C%'
ORDER BY Title , t.Milliseconds;
```
## Query 11

Utilizaremos el operador `JOIN` y filtramos por CustomerId
para saber todo los clientes que han realizado compras, con el operador `WHERE` sabremos cuales de estas compras son superiores a 10$ y por ultimo ordenamos por apellido `(LastName)`.

```sql
SELECT c.FirstName,c.LastName
FROM Customer AS c
JOIN Invoice AS i On c.CustomerId = i.CustomerId
WHERE Total > 10
ORDER BY c.LastName;
```

# Consultas con funciones de agregación

## Query 12
Utilizamos los operadores `AVG`, `MIN` y `MAX` que nos dan la media, mínimo y màximo si le pasamos como informacion una columna `Total`, pero le tenemos que poner el operador `GROUP BY` para que nos agrupe dicha columna.

```sql
SELECT 
    InvoiceId,
    AVG(Total) AS ImporteMedio,
    MIN(Total) AS ImporteMinimo,
    MAX(Total) AS ImporteMaximo
FROM Invoice
GROUP BY InvoiceId;

```

## Query 13

Para mostrar el numero total de `artist` deberemos utilizar el operador `Count` pasandole la columna que quieres contar.

```sql
SELECT COUNT(artistId) AS numArtistas
FROM artist;
```

## Query 14

Utilizamos un operador `JOIN` filtrando por `AlbumId` para saber las canciones asignadas a un album, como queremos saber un album especifico, utilizamos el operador `WHERE` y le pasamos el parametro 'Out Of Time' , por ultimo para saber todas la canciones que pertenecen a este album utilizamos el operador `COUNT`.


```sql
SELECT COUNT(t.TrackId) AS numCanciones
FROM album As a
JOIN track AS t ON a.AlbumId = t.AlbumId
WHERE Title = 'Out Of Time';
```

## Query 15

Para mostrar el numero total de paises en lso que tenemos clientes deberemos utilizar el operador `Count` pasandole la columna que quieres contar `Country` ,filtrando esta columna previamente con el operador `DISTINC` para que no se repitan y te cuente paises de mas.

```sql
SELECT COUNT(DISTINCT Country) AS numPaises
FROM Customer;
```

## Query 16

Deberemos utilizar el operador `JOIN` filtrando por `GenereId`
para saber las canciones que hay asignadas a cada genero, ademas utilizaremos el operador `COUNT` para contar las canciones agrupadas por genero utilizando `GROUP BY`.

```sql
SELECT g.Name AS Genero, COUNT(TrackId) AS numCanciones
FROM track AS t
JOIN genre AS g ON g.GenreId = t.GenreId
GROUP BY g.Name;
```

## Query 17

Utilizamos el operador `JOIN` filtrando por `AlbumId` para saber que canciones pertenecen a cada albun, añadimos el operador `COUNT` para saber el numero de canciones que tiene cada album ,ordenadenados `ORDER BY DESC` de mas grande a mas pequeño. 

```sql
SELECT a.Title AS Album, COUNT(TrackId) AS numCanciones
FROM track AS t
JOIN album AS a ON t.AlbumId = a.AlbumId
GROUP BY a.Title
ORDER BY numCanciones DESC;
```

## Query 18

Hacemos dos `JOIN` para saber cuales son son los las canciones mas compradas y despues hacemos un `COUNT` y un `GROUP BY` para contar todas las canciones y sumar sus precios y saber cuales son las mas vendidas.

```sql
SELECT g.Name AS Genero, COUNT(i.UnitPrice) AS numCompras
FROM invoiceline AS i
JOIN track AS t ON i.TrackId = t.TrackId
JOIN genre AS g ON t.GenreId = g.GenreId
GROUP BY Genero
ORDER BY numCompras DESC;
```

## Query 19

Hacemos lo mismo que el la cosulta 18 pero añadimos `LIMIT`, para filtrar los 6 mas comprados.

```sql
SELECT g.Name AS Genero, COUNT(i.UnitPrice) AS numCompras
FROM invoiceline AS i
JOIN track AS t ON i.TrackId = t.TrackId
JOIN genre AS g ON t.GenreId = g.GenreId
GROUP BY Genero
ORDER BY numCompras DESC
LIMIT 6;
```

## Query 20

Utilizamos el operador `Having`, que sirve para filtrar despues de el operador `GROUP BY`.

```sql
SELECT c.Country, COUNT(c.CustomerId) AS numClientes
FROM Customer AS c
GROUP BY c.Country
HAVING NumClientes >= 5;
```


