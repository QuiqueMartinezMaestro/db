CREATE TABLE Vuelos (
    id_vuelo INT AUTO_INCREMENT PRIMARY KEY,
    origen VARCHAR(50),
    destino VARCHAR(50),
    fecha DATE,
    capacidad INT
);