CREATE TABLE Pasajeros (
    id_pasajero INT AUTO_INCREMENT PRIMARY KEY,
    numero_pasaporte VARCHAR(20),
    nombre_pasajero VARCHAR(100)
);