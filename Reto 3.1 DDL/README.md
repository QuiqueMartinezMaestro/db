# Reto 3.1 DDL - Definición de datos en MySQL

Quique Martínez Maestro.

En este reto trabajamos con las tablas `vuelos`, `pasajeros` y `vuelos_pasajeros` con sus escripts adjuntados. A continuación realizamos una la documentación del proceso de definición de la estructura de datos así como la inserción de algunos registros en la base de datos creada.

Mis Querys se encuentran el el siguiente enlace:

A continuación se mostraremos las tres querys con la que vamos a trabajar, estos tres archivos `sql` nos crean el sistema para gestionar una BD de un sistema de reservas de vuelos:


```sql
CREATE TABLE Vuelos (
    id_vuelo INT AUTO_INCREMENT PRIMARY KEY,
    origen VARCHAR(50),
    destino VARCHAR(50),
    fecha DATE,
    capacidad INT
);
```

```sql
CREATE TABLE Pasajeros (
    id_pasajero INT AUTO_INCREMENT PRIMARY KEY,
    numero_pasaporte VARCHAR(20),
    nombre_pasajero VARCHAR(100)
);
```

```sql
CREATE TABLE Vuelos_Pasajeros (
    id_vuelo INT,
    id_pasajero INT,
    n_asiento INT,
    FOREIGN KEY (id_vuelo) REFERENCES Vuelos(id_vuelo),
    FOREIGN KEY (id_pasajero) REFERENCES Pasajeros(id_pasajero)
);

```

El sistema de reserva de vuelos esta compuesto por tres tablas, la tabla `vuelos` se encarga de almacenar la información relacionada con los vuelos como el origen,destino, capacidad etc, la tabla `pasajeros` almacena la información de los pasajeros como nombre, pasaporte etc y por ultimo la tabla`vuelos_pasajeros` nos une las dos tablas mencionadas anteriormente para poder extraer información adicional como saber que x pasajero està en x vuelo.La división en tres tablas nos permite una estructura mas organizada de la BD para poder administrar la información.

A continuación vamos a enumerar las claves primarias y foraneas de cada tabla explicando su la función que tienen:

* En la tabla `vuelos`, la clave primaria es `id_vuelo`. La clave primaria garantiza que cada registro en la tabla tenga un identificador único, es decir que el `id_vuelo` no se podrá repetir, además en el codigo le hemos asignado un `AUTO_INCREMENT` para 
que incremente su valor de manera automàtica.

* En la tabla `pasajeros`, la clave primaria es `id_pasajero`.

* En la tabla `Vuelos_Pasajeros`, las claves foráneas son `id_vuelo` y `id_pasajero`, que hacen referencia a las claves primarias de las tablas `vuelos` y `pasajeros` respectivamente. Estas claves foráneas establecen la relación entre las tablas que a su misma vez evita incosistencias y referencias a datos que no existen en las tablas `vuelos` y `pasajeros`.Ademas se establece una restricción de integridad para garantizar que no se puedan eliminar vuelos o pasajeros si hay registros asociados en la tabla `vuelos_pasajeros`

Ahora dadas las retricciones asignadas a las tablas con anterioridad vamos a analizar tres posibles casos problematicos para esta BD de gestión de vuelos:

* Cancelar un vuelo: 

```sql
DELETE FROM vuelos_pasajeros WHERE id_vuelo = 1;
```

Ventajas : Si un vuelo se cancela, las restricciones de integridad aseguran que todos los registros en la tabla `vuelos_pasajeros` que hacen referencia a ese vuelo se traten de manera correcta. 

Inconvenientes: Si un vuelo se cancela y no se actualizan los registros en la tabla `vuelos_pasajeros`, podríamos terminar con referencias a un vuelo que ya no existe.


* Asignar un mismo asiento a dos pasajeros diferentes:

```sql
INSERT INTO Vuelos_Pasajeros (id_vuelo, id_pasajero, n_asiento) 
VALUES (123, 1, 10), (123, 2, 10);
```


Ventajas: Las restricciones de integridad aseguran que no se puedan asignar dos pasajeros al mismo asiento en un vuelo.

Inconvenientes: Si no se maneja correctamente podriamos tener dos personas con el mismo asiento lo que produciria que si el vuelo esta lleno una de las dos personas se le tendria que cambiar el vuelo.

* Asignar un pasajero a un vuelo inexistente:

```sql
INSERT INTO vuelos_pasajeros (id_vuelo, id_pasajero, n_asiento) 
VALUES (999, 1, 1);
-- El id_vuelo = 999 en un supuesto no existe en la tabla vuelo
```

Ventajas de las restricciones: Las restricciones de integridad referencial evitan que se asignen pasajeros a vuelos que no existen en la tabla `vuelos`.

Inconvenientes: Si las restricciones estan mas diseñadas el orden en el que se insertan los datos es crucial ya que habria que definir primero el vuelo y despues asignar el pasajero sino se podria asignar un pasajero a un vuelo inexistente.

