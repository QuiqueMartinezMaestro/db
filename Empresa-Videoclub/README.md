# Reto 1: Consultas básicas

Quique Martínez Maestro.

En este reto trabajamos con la base de datos `empresa`y `videoclub`,  que nos viene dada en el fichero `empresa.sql` y `videoclub`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

Mis Querys se encuentran el el siguiente enlace: https://gitlab.com/QuiqueMartinezMaestro/db/-/tree/main/Empresa-Videoclub

## EMPRESA

## Query 1
Para seleccionar el número y descripción de todos los productos, seleccionaremos estos dos atributos, que se corresponden con las columnas `PROD_NUM` y `DESCRIPCIO`, respectivamente, de la tabla `PRODUCTE`. Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT PROD_NUM , DESCRIPCIO
FROM PRODUCTE;
```

## Query 2
Para mostrar las descipciones que contengan la palabra `TENNIS` deberemos utilizar el operador `LIKE`.Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT PROD_NUM , DESCRIPCIO
FROM PRODUCTE
WHERE DESCRIPCIO LIKE '%TENNIS%';
```

## Query 3

Para seleccionar el código ,  nombre ,area y telefono seleccionaremos estos cuatro atributos, que se corresponden con las columnas `CLIENT_COD` , `NOM` ,`AREA ` y `TELEFON` , respectivamente, de la tabla `CLIENT`. Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT CLIENT_COD , NOM, AREA, TELEFON
FROM CLIENT;
```

## Query 4
Pare seleccionar los clientes que no son del áreal telefónica `636` deberemos utilizar el comparador ` !=` en el `WHERE`. Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT CLIENT_COD , NOM, CIUTAT
FROM CLIENT
WHERE AREA != 636;
```

## Query 5

Para seleccionar el codigo, fechas de orden y envio de todos las comandas producto, seleccionaremos estos tres atributos, que se corresponden con las columnas `COD_NUM` , `DATA_TRAMESA` y `COM_DATA`, respectivamente, de la tabla `COMANDA`. Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT COM_NUM , DATA_TRAMESA, COM_DATA
FROM COMANDA;
```

## VIDEOCLUB

## Query 6
Para seleccionar el número y teléfono de todos los productos, seleccionaremos estos dos atributos, que se corresponden con las columnas `NOM` y `TELEFON`, respectivamente, de la tabla `CLIENT`. Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT NOM, TELEFON
FROM CLIENT;
```

## Query 7
Para seleccionar las fechas y imported de todas las facturas, seleccionaremos estos dos atributos, que se corresponden con las columnas `DATA` y `IMPORT`, respectivamente, de la tabla `FACTURA`. Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT DATA, IMPORT
FROM FACTURA;
```

## Query 8
Para seleccionar las descripciones facturadas que su codigo sea `3`, seleccionaremos este atributo, que se corresponden con las columna `DESCRIPCIO`, respectivamente, de la tabla `DETALLFACTURA`  y utilizaremos un el comparador `=` en el `WHERE`. Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT DESCRIPCIO
FROM DETALLFACTURA
WHERE CodiFactura = 3;
```

## Query 9

Para sacar una lista de las facturas que este ordenada de manera descendente `DESC`, tendremos que utilizar el operador `ORDER BY` que sirbe para ordenar tanto de manera `DESC` como `ASC`.
Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT *
FROM FACTURA
ORDER BY IMPORT DESC;
```

## Query 10

Para mostrar los nombres de los actores  que comiencen por la `X` deberemos utilizar el operador `LIKE`.Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT *
FROM ACTOR
WHERE NOM LIKE 'X%';
```