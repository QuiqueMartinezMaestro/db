# Reto 1: Consultas básicas

Quique Martínez Maestro.

En este reto trabajamos con la base de datos `sanitat`, que nos viene dada en el fichero `sanitat.sql`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

Mis Querys se encuentran el el siguiente enlace: https://gitlab.com/QuiqueMartinezMaestro/db/-/tree/main/Sanitat

## Query 1
Para seleccionar el número, nombre y teléfono de todos los hospitales existentes, seleccionaremos estos tres atributos, que se corresponden con las columnas `HOSPITAL_COD`, `NOM`, y `TELEFON`, respectivamente, de la tabla `HOSPITAL`. Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT  HOSPITAL_COD AS NUMERO, NOM, TELEFON
FROM    HOSPITAL;
```

## Query 2

Para mostrar los hospitales que contengan una `A` en la segunda posición de su nombre deberemos 
utilizar el operador `LIKE`.Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT HOSPITAL_COD AS NUMERO, NOM, TELEFON
FROM HOSPITAL
WHERE NOM LIKE '_a%';
```

## Query 3

Para seleccionar todos los trabajadores existentes, seleccionaremos estos cuatro  atributos, que se corresponden con las columnas `HOSPITAL_COD`, `SALA_COD`, `EMPLEAT_NO` y `COGNOM`  respectivamente, de la tabla `HOSPITAL`. Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT HOSPITAL_COD, SALA_COD, EMPLEAT_NO, COGNOM
FROM PLANTILLA;
```

## Query 4

Para mostrar los trabajadores cuyo turno sea de noche tendremos que pregunatar en el `WHERE` si
el turno es nocturo --> `(N)`.Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT HOSPITAL_COD, SALA_COD, EMPLEAT_NO, COGNOM
FROM PLANTILLA
WHERE TORN = 'N';
```

## Query 5
Para mostrar los enfermos nacidos en 1960 deberemos utilizar el operador `YEAR` , el cual utilizaremos dentro del  `WHERE` para realizar la comparación. Lo llevaremos a cabo con la siguiente sentencia SQL:
```sql
SELECT *
FROM MALALT
WHERE YEAR(DATA_NAIX) = '1960';
```

## Query 6
Para mostrar los enfermos nacidos a partir 1960 deberemos utilizar el operador `YEAR` , el cual utilizaremos dentro del  `WHERE` para realizar la comparación. Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT *
FROM MALALT
WHERE YEAR(DATA_NAIX) >= '1960';
```